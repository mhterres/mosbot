#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
	queue.py
"""

import re
import os
import sys
import logging

from asterisk import manager
from yapsy.IPlugin import IPlugin

sys.path.insert(1,'%s/../' % os.path.dirname(os.path.realpath(__file__)))
from mosbot.astqueue import astQueue
from mosbot.functions_support import getAsteriskRealtimeInformation


class queue(IPlugin):

	def __init__(self):

		self.name="queue"
		self.type="both"
		self.regexp=["^[qQ]ueue (.+)"]
		self.re = [ re.compile(expr) for expr in self.regexp ]
		self.syntax="queue <room name>"
		self.description="enter/leave an asterisk queue."
		self.answers="xmpp"

	def match(self,cmd):

		for regexp in self.re:

			m = regexp.match(cmd)

			if m:

				return m

		return False

	def execute(self,data):

		cfg=data['bot'].config

		ami = manager.Manager()

		# connect to the manager
		try:
			ami.connect(cfg.ast_srv)
			ami.login(cfg.ast_mgr_user,cfg.ast_mgr_pwd)

		except manager.ManagerSocketException, (errno, reason):
		   logging.error("Error connecting to the manager: %s" % reason)
		   sys.exit(1)
		except manager.ManagerAuthException, reason:
		   logging.error("Error logging in to the manager: %s" % reason)
		   sys.exit(1)
		except manager.ManagerException, reason:
		   logging.error("Error: %s" % reason)
		   sys.exit(1)
		else:

			for expr in self.regexp:

				regexp = re.compile(expr)
				m = regexp.match(data['cmd'])

				if m:

					break

			asterisk_queue = m.groups()[0]

			if data['msg_type']=='chat':

				msg_jid=data['msg_to'].bare.split("/")[0]
			else:

				msg_jid="%s@%s" % (data['msg_from'].split("/")[0],cfg.xmpp_domain)

			realtime_data=getAsteriskRealtimeInformation(msg_jid)
			extension_number=realtime_data[0] 
			extension_jid=realtime_data[1] 
			extension_callerid=realtime_data[2] 

			if extension_number=="" or extension_jid=="":

				output = "Your jid (%s) needs to be linked with an asterisk extension." % msg_jid
				data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type'])
				return False
			else:

				myqueue=astQueue(asterisk_queue,ami)

				if myqueue.isavailable:

					myqueue.getmembers()

					if myqueue.ismember(extension_number):

						output=myqueue.removemember(extension_number)
						data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type'])
						return True
					else:

						output=myqueue.addmember(extension_number)
						data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type'])
						return True

				else:

					output = "Queue %s does not exist." % asterisk_queue
					data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type'])
					return False
		

