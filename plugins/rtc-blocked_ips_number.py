#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
	rtc-blocked_ips_number.py
"""

import re
from yapsy.IPlugin import IPlugin
from subprocess import Popen, PIPE, STDOUT

class rtc_blocked_ips_number(IPlugin):

	def __init__(self):

		self.name="rtc-blocked ips number"
		self.type="both"
		self.regexp=["^([sS]how )?[nN]umber of blocked [iI][pP][sS]"]
		self.re = [ re.compile(expr) for expr in self.regexp ]
		self.syntax="show number of blocked ips"
		self.description="show number of IPs blocked in RTC server."
		self.answers="both"

	def match(self,cmd):

		for regexp in self.re:

			m = regexp.match(cmd)

			if m:

				return m

		return False

	def execute(self,data):

		cmd = '%s/rtc_number_blocked_ips.sh' % data['bot'].scripts_path
		p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
		output = "%s blocked IPs." % p.stdout.read()

		data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type'])

		return True

