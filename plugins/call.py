#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
	call.py
"""

import re
import os
import sys
import logging

from yapsy.IPlugin import IPlugin
from asterisk import manager

sys.path.insert(1,'%s/../' % os.path.dirname(os.path.realpath(__file__)))
from mosbot.functions_support import getAsteriskRealtimeInformation


class call(IPlugin):

	def __init__(self):

		self.name="call"
		self.type="both"
		self.regexp=["^[cC]all (.+)"]
		self.re = [ re.compile(expr) for expr in self.regexp ]
		self.syntax="call <number>"
		self.description="make calls to extensions (try to call 10 or 99). UNDER DEVELOPMENT"
		self.answers="xmpp"

		self.call_context="call"
		

	def match(self,cmd):

		for regexp in self.re:

			m = regexp.match(cmd)

			if m:

				return m

		return False

	def execute(self,data):
		
		cfg=data['bot'].config

		if data['msg_type']=='chat':

			msg_jid=data['msg_to'].bare.split("/")[0]
		else:

			msg_jid="%s@%s" % (data['msg_from'].split("/")[0],cfg.xmpp_domain)

		realtime_data=getAsteriskRealtimeInformation(msg_jid)
		extension_number=realtime_data[0]
		extension_jid=realtime_data[1]
		extension_callerid=realtime_data[2]

		if extension_number=="" or extension_jid=="":

			output = "Your jid (%s) needs to be linked with an asterisk extension." % msg_jid
			data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type'])
			return False
		else:

			number = data['match'].groups()[0]
			ami = manager.Manager()

			try:
				ami.connect(cfg.ast_srv)
				ami.login(cfg.ast_mgr_user,cfg.ast_mgr_pwd)

			except manager.ManagerSocketException, (errno, reason):
				logging.error("Error connecting to the manager: %s" % reason)
				sys.exit(1)
			except manager.ManagerAuthException, reason:
				logging.error("Error logging in to the manager: %s" % reason)
				sys.exit(1)
			except manager.ManagerException, reason:
				logging.error("Error: %s" % reason)
				sys.exit(1)
			else:

				cDict = {'Action':'Originate'}
				cDict['Channel'] = "SIP/%s" % number
				cDict['Exten'] = "SIP/%s" % extension_number
				cDict['CallerID'] = 'Call from %s' % extension_number
				cDict['Context'] = self.call_context
				cDict['Priority'] = '1'
				cDict['Async'] = 'true'

				response = ami.send_action(cDict)

				if response.get_header('Response') == 'Error':

					output="ERROR: It's not possible to dial."
					data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type'])

					return False
				else:

					output="Dialing to %s" % number
					data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type'])

					return True

		output="ERROR: It's not possible to dial."
		data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type'])

		return False
		
