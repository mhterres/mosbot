#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
	rtc-how_many_downloads.py
"""

import re
from yapsy.IPlugin import IPlugin
from subprocess import Popen, PIPE, STDOUT

class rtc_how_many_downloads(IPlugin):

	def __init__(self):

		self.name="rtc-how many downloads"
		self.type="both"
		self.regexp=["^[hH]ow many downloads (.+) have\?"]
		self.re = [ re.compile(expr) for expr in self.regexp ]
		self.syntax="how many downloads (astdemo|b9|serverinfo) have?"
		self.description="show how many downloads MOS projects have."
		self.answers="both"

	def match(self,cmd):

		for regexp in self.re:

			m = regexp.match(cmd)

			if m:

				return m

		return False

	def execute(self,data):

		project=data['cmd'].split(" ")[3].lower()

		if project in "astdemo|b9|serverinfo":

			cmd = 'ssh root@rtc \'/usr/bin/wc -l /var/log/%s_downloads.log | cut -f 1 -d " "\'' % project
			p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
			output = "%s has %s downloads." % (project,p.stdout.read().replace("\r","").replace("\n",""))

			data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type'])

		else:
			output="Project %s is invalid." % project
			data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type'])

		return True

