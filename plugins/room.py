#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
	room.py
"""

import re
import sleekxmpp
from yapsy.IPlugin import IPlugin

class sala(IPlugin):

	def __init__(self):

		self.name="room"
		self.type="both"
		self.regexp=["^[jJ]oin room (.+)","^[jJ]oin (.+)","^[rR]oom (.+)"]
		self.re = [ re.compile(expr) for expr in self.regexp ]
		self.syntax="join room <roomname>"
		self.description="join a conference room."
		self.answers="xmpp"
		
	def match(self,cmd):

		for regexp in self.re:

			m = regexp.match(cmd)

			if m:

				return m

		return False

	def execute(self,data):

		cfg=data['bot'].config

		for expr in self.regexp:

			regexp = re.compile(expr)
			m = regexp.match(data['cmd'])

			if m:

				break

		if data['msg_type']=='chat':

			msg_jid=data['msg_to'].bare.split("/")[0]
		else:

			msg_jid="%s@%s" % (data['msg_from'].split("/")[0],cfg.xmpp_domain)

		room_name = m.groups()[0]
		data['bot'].plugin['xep_0045'].joinMUC(room_name + '@' + data['bot'].confsrv, data['bot'].nick, wait=True)
		data['bot'].plugin['xep_0249'].send_invitation(
			'%s@%s' % (data['msg_from'],cfg.xmpp_domain),
			room_name + "@" + data['bot'].confsrv, 
			None , '%s is inviting you to join room %s' % (data['msg_from'],room_name))

		output="Ok %s, let's go to room %s." % ( data['msg_from'], room_name )
		data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type'])

		return True

