#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
	who_are_you.py
"""

import re
from yapsy.IPlugin import IPlugin

class who_are_you(IPlugin):

	def __init__(self):

		self.name="who are you"
		self.type="both"
		self.regexp=["^[wW]ho (are|r) (you\u)?"]
		self.re = [ re.compile(expr) for expr in self.regexp ]
		self.syntax="who are you?"
		self.description="show bot informations."
		self.answers="both"

	def match(self,cmd):

		for regexp in self.re:

			m = regexp.match(cmd)

			if m:

				return m

		return False

	def execute(self,data):

		output  = "Hi.\n"
		output += "My name is David and I'm the bot of Mundo Open Source conference room.\n";
		output += "I'm a bot written in python, and I'm named after David from Artificial Intelligente movie.\n"
		output += "http://en.wikipedia.org/wiki/A.I._Artificial_Intelligence\n"
		output += "http://files.mundoopensource.com.br/david_ia.jpg\n\n"
		output += "Feel free to contact me anytime you want."

		data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type'])
		
		return True

