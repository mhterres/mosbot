#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
	tell.py
"""

import re
from yapsy.IPlugin import IPlugin

class tell(IPlugin):

	def __init__(self):

		self.name="tell"
		self.type="both"
		self.regexp=["^[tT]ell ([a-z]+) that (.+)","^[tT]ell to ([a-z]+) that (.+)"]
		self.re = [ re.compile(expr) for expr in self.regexp ]
		self.syntax="tell <user> that <message>"
		self.description="send/leave a message to an user."
		self.answers="both"

	def match(self,cmd):

		for regexp in self.re:

			m = regexp.match(cmd)

			if m:

				return m

		return False

	def execute(self,data):

		cfg=data['bot'].config

		for expr in self.regexp:

			regexp = re.compile(expr)
			m = regexp.match(data['cmd'])

			if m:

				break

		tell_to, tell_msg = m.groups()
		data['bot'].send_message(mto = data['msg_to'],
					mbody = "Ok %s, I'll tell %s that." % (data['msg_from'], tell_to), 
					mtype = data['msg_type'])

		data['bot'].send_message(mto = '%s@%s' % (tell_to,cfg.xmpp_domain),
							mbody = '%s, %s ask me to tell you that %s' % (tell_to, data['msg_from'], tell_msg),
							mtype = 'chat')

		return True
		
