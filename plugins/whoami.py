#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
	whoami.py
"""

import re
import os
import sys
from yapsy.IPlugin import IPlugin

sys.path.insert(1,'%s/../' % os.path.dirname(os.path.realpath(__file__)))
from mosbot.functions_support import getAsteriskRealtimeInformation


class whoami(IPlugin):

	def __init__(self):

		self.name="who am i"
		self.type="both"
		self.regexp=["^[wW]ho am [iI]?"]
		self.re = [ re.compile(expr) for expr in self.regexp ]
		self.syntax="who am i?"
		self.description="show who am I."
		self.answers="xmpp"

	def match(self,cmd):

		for regexp in self.re:

			m = regexp.match(cmd)

			if m:

				return m

		return False

	def execute(self,data):

		cfg=data['bot'].config
		
		if data['msg_type']=='chat':

			msg_jid=data['msg_to'].bare.split("/")[0]
		else:

			msg_jid="%s@%s" % (data['msg_from'].split("/")[0],cfg.xmpp_domain)

		realtime_data=getAsteriskRealtimeInformation(msg_jid)
		extension_number=realtime_data[0]
		extension_jid=realtime_data[1]
		extension_callerid=realtime_data[2]

		output ="My Jabber ID is %s.\n" % extension_jid
		output+="My extension is %s.\n" % extension_number
		output+="My CallerID is %s." % extension_callerid

		data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type'])
		
		return True

