#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
	wpsearch.py
"""

import re
import os
import sys
import sleekxmpp
from yapsy.IPlugin import IPlugin

sys.path.insert(1,'%s/../' % os.path.dirname(os.path.realpath(__file__)))
from mosbot.config import Config
from mosbot.wp import WP

class wpsearch(IPlugin):

	def __init__(self):

		cfg=Config()

		self.name="wpsearch"
		self.type="both"
		self.regexp=["^[wW]psearch (.+)"]
		self.re = [ re.compile(expr) for expr in self.regexp ]
		self.syntax="wpsearch <information>"
		self.description="search in a WordPress site (%s)." % cfg.wp_name
		self.answers="both"

	def match(self,cmd):

		for regexp in self.re:

			m = regexp.match(cmd)

			if m:

				return m

		return False

	def execute(self,data):

		wp=WP(data['bot'].config)

		params=(data['cmd'].strip()).split(" ")
		searchStr=""

		if len(params) > 1:

			i=0

			for value in params:

				if i>0:

					if len(searchStr)>0:

						searchStr+=" "

					searchStr+=value

				i=i+1

			output=wp.search(searchStr)
			data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type'])

		else:

			output="You need to inform something to search."
			data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type'])

		return True

