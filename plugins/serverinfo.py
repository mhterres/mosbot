#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
	serverinfo.py
"""

import re
import sleekxmpp
from yapsy.IPlugin import IPlugin

class serverinfo(IPlugin):

	def __init__(self):

		self.name="serverinfo"
		self.type="both"
		self.regexp=["^[sS]erverinfo (.+)"]
		self.re = [ re.compile(expr) for expr in self.regexp ]
		self.syntax="serverinfo <command>"
		self.description="run serverinfo plugin commands."
		self.answers="xmpp"

	def match(self,cmd):

		for regexp in self.re:

			m = regexp.match(cmd)

			if m:

				return m

		return False

	def execute(self,data):

		params=(data['cmd'].strip()).split(" ")
		runcmd=""

		if len(params) > 1:

			i=0

			for value in params:

				if i>0:

					if len(runcmd)>0:

						runcmd+=" "

					runcmd+=value

				i=i+1

			output=data['bot'].serverinfoplugin.runCmd(runcmd)
			data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type'])

		else:

			output="You need to inform a command to run."
			data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type'])


		return True
