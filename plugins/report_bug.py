#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
	report_bug.py
"""

import re
import smtplib
from yapsy.IPlugin import IPlugin
from email.mime.text import MIMEText


class report_bug(IPlugin):

	def __init__(self):

		self.name="report bug"
		self.type="both"
		self.regexp=["^[rR]eport bug (.+)"]
		self.re = [ re.compile(expr) for expr in self.regexp ]
		self.syntax="report bug <bug>"
		self.description="report a bug."
		self.answers="both"

		self.email_from="bugs@mundoopensource.com.br"
		self.email_destination="mhterres@gmail.com"
		self.xmpp_destination="mhterres@jabber.mundoopensource.com.br"

	def match(self,cmd):

		for regexp in self.re:

			m = regexp.match(cmd)

			if m:

				return m

		return False

	def execute(self,data):

		bug=data['match'].groups()[0]

		send_email=False
		send_xmpp=False

		if len(self.email_destination)>0:

			send_email=True

		if len(self.xmpp_destination)>0:

			send_xmpp=True

		if send_xmpp:

			data['bot'].send_message(mto=self.xmpp_destination,mbody="Bug reported by %s: %s" % (data['msg_from'],bug),mtype='chat')
		
		if send_email:

			msg = MIMEText(bug)

			msg['Subject'] = 'Bug report from %s' % data['msg_from']
			msg['From'] = self.email_from
			msg['To'] = self.email_destination

			s = smtplib.SMTP('localhost')
			s.sendmail(self.email_from, [self.email_destination], msg.as_string())
			s.quit()
		
		return True

