#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
	rtc-blocked_ips_number.py
"""

import re
import logging

from yapsy.IPlugin import IPlugin
from subprocess import Popen, PIPE, STDOUT

class rtc_is_blocked_ip(IPlugin):

	def __init__(self):

		self.name="rtc-is blocked ip"
		self.type="both"
		self.regexp=["^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]) is blocked\?$"]
		self.re = [ re.compile(expr) for expr in self.regexp ]
		self.syntax="<IP> is blocked?"
		self.description="show ip an IP is blocked in RTC server."
		self.answers="both"

	def match(self,cmd):

		for regexp in self.re:

			m = regexp.match(cmd)

			if m:

				return m

		return False

	def execute(self,data):

		ip=data['cmd'].split(' ')[0]

		cmd = '%s/rtc_ip_is_blocked.sh %s' % (data['bot'].scripts_path,ip)
		p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
		p.communicate()[0]
		response = p.returncode

		if response==0:

			 output = "%s is blocked." % ip
			 data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type'])

		else:

			output = "%s is not blocked." % ip
			data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type'])


		return True

