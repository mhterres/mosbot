#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    config.py
	MOSBot configuration class
    2014/12/13
    Marcelo Hartmann Terres <mhterres@gmail.com>
"""

import os
import sys
import ConfigParser

class Config:

	def __init__(self):

		self.description = 'MOSbot configurations'

		configuration = ConfigParser.RawConfigParser()

		configuration = ConfigParser.RawConfigParser()
		configuration.read('/etc/mosbot/bot_config.ini')

		self.botname=configuration.get('general','botname')
		self.logfile=configuration.get('general','logfile')
		self.debug=configuration.get('general','debug')

		# xmpp
		self.jid=configuration.get('xmpp','jid')
		self.jid_pwd=configuration.get('xmpp','jid_pwd')
		self.xmpp_domain=configuration.get('xmpp','xmpp_domain')
		self.roomname=configuration.get('xmpp','roomname')
		self.confsrv=configuration.get('xmpp','confsrv')

		# dbpgsql
		self.dbpgsql_host=configuration.get('dbpgsql','dbpgsql_host')
		self.dbpgsql_database=configuration.get('dbpgsql','dbpgsql_database')
		self.dbpgsql_user=configuration.get('dbpgsql','dbpgsql_user')
		self.dbpgsql_pwd=configuration.get('dbpgsql','dbpgsql_pwd')

		# wp
		self.wp_name=configuration.get('wp','wp_name')
		self.wp_host=configuration.get('wp','wp_host')
		self.wp_database=configuration.get('wp','wp_database')
		self.wp_db_user=configuration.get('wp','wp_db_user')
		self.wp_db_pwd=configuration.get('wp','wp_db_pwd')
		self.wp_url=configuration.get('wp','wp_url')

		# serverinfo
		self.serverinfo_host=configuration.get('serverinfo','serverinfo_host')
		self.serverinfo_port=configuration.get('serverinfo','serverinfo_port')

		# asterisk
		self.ast_srv=configuration.get('asterisk','ast_srv')
		self.ast_mgr_port=configuration.get('asterisk','ast_mgr_port')
		self.ast_mgr_user=configuration.get('asterisk','ast_mgr_user')
		self.ast_mgr_pwd=configuration.get('asterisk','ast_mgr_pwd')
		self.ast_db_host=configuration.get('asterisk','ast_db_host')
		self.ast_db_name=configuration.get('asterisk','ast_db_name')
		self.ast_db_user=configuration.get('asterisk','ast_db_user')
		self.ast_db_pwd=configuration.get('asterisk','ast_db_pwd')
		self.ast_sip_table=configuration.get('asterisk','ast_sip_table')
		self.ast_jid_field=configuration.get('asterisk','ast_jid_field')

		# whatsapp
		self.whatsapp_id=configuration.get('whatsapp','whatsapp_id')
		self.whatsapp_pwd=configuration.get('whatsapp','whatsapp_pwd')

