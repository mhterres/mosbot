#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    dbpgsql.py
    DB Postgres class for interacting with mucbot database
	see db/pgsql.sql for db schema
    2014/12/15
    Marcelo Hartmann Terres <mhterres@gmail.com>
"""
import psycopg2
import psycopg2.extras

class DBPgsql:

	def __init__(self,botconfig):

		self.description = 'Postgresql Database Operations'

		self.dsn = 'dbname=%s host=%s user=%s password=%s' % (botconfig.dbpgsql_database,botconfig.dbpgsql_host,botconfig.dbpgsql_user,botconfig.dbpgsql_pwd)

		self.conn = psycopg2.connect(self.dsn)

	def get_LastLoggedUsers(self,items):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs.execute("SELECT nick,operation,date FROM users_logs ORDER BY date desc limit %s;" % items)

		if not curs.rowcount:
			
			returnmsg = "There is no enough information right now."
		else:

			returnmsg=""

			rec=curs.fetchone()
			while rec is not None:
				returnmsg += "\nUser " + rec['nick'] + " " + rec['operation'] + " at " + rec['date'].strftime("%d/%m/%Y %H:%M:%S")
				rec=curs.fetchone()

		curs.close()

		return (returnmsg)

	def save_message(self,nick,msg_from,msg_to,message):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs.execute("INSERT INTO muc_chat (nick, msg_from, msg_to, message) VALUES(%s, %s , %s, %s);", (nick,msg_from,msg_to,message))
		self.conn.commit()
		curs.close()


	def enter_room(self,nick,jid):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs.execute("INSERT INTO users_logs (nick, jid, operation) VALUES(%s, %s, %s);", (nick,jid,'logged in'))
		self.conn.commit()
		curs.close()

	def leave_room(self,nick,jid):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs.execute("INSERT INTO users_logs (nick, jid, operation) VALUES(%s,%s, %s);", (nick,jid,'logged off'))
		self.conn.commit()
		curs.close()

	def list_history(self,lines):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs.execute("SELECT date,nick,message from muc_chat ORDER BY date DESC limit %s;", (lines,))

		returnmsg=""

		rec=curs.fetchone()

		while rec is not None:

			returnmsg += "\n[" + rec['date'].strftime("%d/%m/%Y %H:%M:%S") + "] - " + rec['nick'] + ": " + rec['message']

			rec=curs.fetchone()

		curs.close()

		return (returnmsg)

