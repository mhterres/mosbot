#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    wp.py
    Class to access WordPress Site - MySQL
    2014/12/16
    Marcelo Hartmann Terres <mhterres@gmail.com>
"""
import mysql.connector

class WP:

	def __init__(self,botconfig):

		self.description = 'WP database access'

		botconfig = Config()
	
		self.conn = mysql.connector.connect(user='%s' % botconfig.wp_db_user, password='%s' % botconfig.wp_db_pwd, host='%s' % botconfig.wp_host, database='%s' % botconfig.wp_database)

		self.wp_url=botconfig.wp_url
		self.wp_name=botconfig.wp_name

	def search(self,searchStr):

		curs = self.conn.cursor()
		query="SELECT id,post_title,post_name FROM wp_posts WHERE post_content LIKE '%" + searchStr + "%' AND post_status='publish' ORDER BY id DESC LIMIT 10;" 
		curs.execute(query)
		returnmsg=""

		rows=curs.fetchall()
		
		i=0

		for row in rows:

			if i==0:

				returnmsg="Results found for %s:" % searchStr 

			i=i+1
			
			returnmsg += "\n%s - %s/%s" % (row[1],self.wp_url,row[2])

		curs.close()

		if returnmsg=="":

			returnmsg="Expression %s not found in %s." % (searchStr,self.wp_name)

		return (returnmsg)

