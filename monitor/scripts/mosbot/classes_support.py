# -*- coding: utf-8 -*-

import time
import socket

from mosbot.config import Config

class connectAMI:

  def __init__(self):

	myConfigs = Config()

	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect((myConfigs.ast_srv,int(myConfigs.ast_mgr_port)))

	s.send('Action: Login\nUsername: '+myConfigs.ast_mgr_user+'\nSecret: '+myConfigs.ast_mgr_pwd+'\nEvents: off\n\n')
	time.sleep (0.1)
	data = s.recv(2048)

	self.socket = s

class ManagerDict:

  def __init__(self,text):

	# split the text
	lines = text.split('\n')

	d={}

	for item in lines:

	  data=item.split(":",1)

	  try:

		title=data[0]
	  except:

		title=""

	  try:

		value=data[1]
	  except:

		value=""


	  d.update({title:value})

	self.dicti=d

  def getitem(self,index):

	i=self.dicti[index]
	i=i.replace(" ","\ ")
	return i

class ManagerDictEvents:

  def __init__(self,text,myevent):

	self.description = "Generate dictionary of a specific event."

	# split the text
	lines = text.split('\n')

	i=0

	dicio={}
	events=[]

	fulldicio={}
	dicioitem=0

	if text.find('Event: ' + myevent) == -1:

	  self.isvalid=False
	else:

	  self.isvalid=True

	  self.events=[]
	  self.items=0
	  self.fulldicti={}
	  self.dictiitems=0

	  for myitem in lines:

		data=myitem.split(":",1)

		try:

		  title=data[0]
		except:

		  title=""

		try:

		  value=data[1]
		except:

		  value=""

		if (title == "Event" and value.find(myevent) > 0 and len(dicio) == 0):

		  i+=1
		  events.append({})
		  dicioitem+=1
		  fulldicio.update({title:value})

		  dicio.update({title:value})
		  events[i-1].update({title:value})

		elif (title == "Event" and len(dicio)>0):

		  dicioitem+=1
		  fulldicio.update({title:value})

		  if len(dicio) > 0:

			events[i-1].update({title:value})

			dicio.clear()
			dicio={}

		  if value.find(myevent) > 0:

			i+=1
			events.append({})
			dicioitem+=1
			fulldicio.update({title:value})

			events[i-1].update({title:value})
			dicio.update({title:value})

		elif (title != "" and len(dicio)>0):

		  dicioitem+=1
		  fulldicio.update({title:value})

		  events[i-1].update({title:value})
		  dicio.update({title:value})

	  self.items=i
	  self.events=events
	  self.dicti=fulldicio
	  self.dictiitems=dicioitem
	  self.text=text


