#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
	serverinfo.py
	ServerInfo (http://www.mundoopensource.com.br/serverinfo-plugin-openfire/) access class
	2014/12/24
	Marcelo Hartmann Terres <mhterres@gmail.com>
"""

import time
import socket

class ServerInfoPlugin:

	def __init__(self,botconfig):

		self.description = 'ServerInfo Interaction Class'

		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect((botconfig.serverinfo_host,int(botconfig.serverinfo_port)))
		self.socket=s
	
	def runCmd(self,cmd):

		self.socket.send(cmd + "\n")
		time.sleep (0.2)
		data = self.socket.recv(65536)

		returnmsg ="Result for %s:\n" % cmd
		returnmsg+=data.replace("\n","").replace("\r","")

		return returnmsg

