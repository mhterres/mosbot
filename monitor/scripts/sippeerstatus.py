#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import time

from mosbot.classes_support import connectAMI
from mosbot.classes_support import ManagerDict

sip = sys.argv[1]

myConnect=connectAMI()
s = myConnect.socket

s.send('Action: SIPshowpeer\nPeer: %s\n\n' % sip)
time.sleep (0.1)

data = s.recv(65536)

mgrdict=ManagerDict(data)

if data.find("Peer %s not found" % sip) > 0:

	returnmsg = "UNAVAILABLE"
else:
	returnmsg = mgrdict.getitem('Status').split("(")[0].replace("\\","").replace("\r","").replace("\n","")

s.close()

print returnmsg.strip()

