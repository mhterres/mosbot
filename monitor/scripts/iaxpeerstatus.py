#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import time

from mosbot.classes_support import connectAMI
from mosbot.classes_support import ManagerDict
from mosbot.classes_support import ManagerDictEvents

iax = sys.argv[1]

myConnect=connectAMI()
s = myConnect.socket

s.send('Action: IAXpeerlist\n\n')
time.sleep (0.1)

data = s.recv(65535)

mgrdictev=ManagerDictEvents(data,'PeerEntry')

returnmsg=""

if not mgrdictev.isvalid:

	returnmsg = "UNAVAILABLE"

else:

	found=False

	for event in mgrdictev.events:

	  if not found:

		if (event['ObjectName'].replace("\\","").replace("\r","").replace("\n","")).strip() == iax.strip():

		  found=True
		  returnmsg=event['Status'].split("(")[0].replace("\\","").replace("\r","").replace("\n","")
		  break

	if not found:

	  returnmsg = "UNAVAILABLE"

s.close()

print returnmsg.strip()

