#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
	whatsapp.py
"""

import re
import os
import sys
import logging

import multiprocessing

sys.path.insert(1,os.path.dirname(os.path.realpath(__file__)))
from send import SendStack

sys.path.insert(1,"%s/receive" % os.path.dirname(os.path.realpath(__file__)))
from moswareceive import mosWAReceive

sys.path.insert(1,'%s/../' % os.path.dirname(os.path.realpath(__file__)))
from mosbot.functions_support import getAsteriskRealtimeInformation

class WhatsApp():

	def __init__(self,bot):

		self.bot=bot

	def whatsappSend(self,cmd,data):

		cfg=data['bot'].config

		if data['msg_type']=='chat':

			msg_jid=data['msg_to'].bare.split("/")[0]
		else:

			msg_jid="%s@%s" % (data['msg_from'].split("/")[0],cfg.xmpp_domain)

		realtime_data=getAsteriskRealtimeInformation(msg_jid)
		extension_number=realtime_data[0]
		extension_jid=realtime_data[1]
		extension_callerid=realtime_data[2]

		if extension_number=="" or extension_jid=="":

			output = "Your jid (%s) needs to be linked with an asterisk extension." % msg_jid
			data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type'])
			return False

		params=(data['cmd'].strip()).split(" ") 
		number=params[2]
		message=""

		if len(params) > 2: 

			if len(params) > 3:
 
				i=1
 
				for value in params: 
 
					if i>3: 
 
						if len(message)>0: 
 
							message+=" " 

						message+=value
 
					i=i+1 

				logging.debug("WhatsApp: Sending message %s to %s" % (message,number))

				p=multiprocessing.Process(target=self.sendmsg,args=(number,message,cfg))
				p.daemon=False

				p.start()
				p.join()

				output="WhatsApp message sent to %s: %s." % (number,message)
				data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type']) 

				self.bot.dbpgsql.saveSendWAMessage(number,message)
			else:

				output="You need to inform message." 
				data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type']) 
				return False
		else: 

			output="You need to inform number and message." 
			data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type']) 
			return False

		return True

	def whatsappList(self,cmd,data):

		cfg=data['bot'].config

		if data['msg_type']=='chat':

			msg_jid=data['msg_to'].bare.split("/")[0]
		else:

			msg_jid="%s@%s" % (data['msg_from'].split("/")[0],cfg.xmpp_domain)

		realtime_data=getAsteriskRealtimeInformation(msg_jid)
		extension_number=realtime_data[0]
		extension_jid=realtime_data[1]
		extension_callerid=realtime_data[2]

		if extension_number=="" or extension_jid=="":

			output = "Your jid (%s) needs to be linked with an asterisk extension." % msg_jid
			data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type'])
			return False

		output=self.bot.dbpgsql.listWAMessagesWaitingAnswers()

		data['bot'].send_message(mto=data['msg_to'],mbody=output,mtype=data['msg_type']) 

		return True


	def sendmsg(self,number,message,cfg):

		stacklocal = SendStack((cfg.whatsapp_id,cfg.whatsapp_pwd),[([number,message])])
		stacklocal.start()

		return "Message %s sent to %s." % (message,number)

	def start(self):

		logging.debug("WhatsApp: Initializing Receiving...")

		stacklocal = mosWAReceive(self.bot)
		stacklocal.start()

