import logging

from yowsup.layers.interface                           import YowInterfaceLayer, ProtocolEntityCallback
from yowsup.layers.protocol_messages.protocolentities  import TextMessageProtocolEntity
from yowsup.layers.protocol_receipts.protocolentities  import OutgoingReceiptProtocolEntity
from yowsup.layers.protocol_acks.protocolentities      import OutgoingAckProtocolEntity

class ReceiveLayer(YowInterfaceLayer):

    @ProtocolEntityCallback("message")
    def onMessage(self, messageProtocolEntity):
        #send receipt otherwise we keep receiving the same message over and over

        if True:
            receipt = OutgoingReceiptProtocolEntity(messageProtocolEntity.getId(), messageProtocolEntity.getFrom(), 'read', messageProtocolEntity.getParticipant())

#            outgoingMessageProtocolEntity = TextMessageProtocolEntity(
#                messageProtocolEntity.getBody(),
#                to = messageProtocolEntity.getFrom())

            logging.debug("WhatsApp: message event")

            print "WhatsApp message received from %s: %s" % (messageProtocolEntity.getFrom().split('@')[0],messageProtocolEntity.getBody())

            self.toLower(receipt)
#            self.toLower(outgoingMessageProtocolEntity)

    @ProtocolEntityCallback("receipt")
    def onReceipt(self, entity):

        logging.debug("WhatsApp: receipt event")

        ack = OutgoingAckProtocolEntity(entity.getId(), "receipt", "delivery", entity.getFrom())
        self.toLower(ack)
