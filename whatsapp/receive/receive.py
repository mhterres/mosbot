#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import logging

from layer import ReceiveLayer
from yowsup.layers.auth                        import YowAuthenticationProtocolLayer
from yowsup.layers.protocol_messages           import YowMessagesProtocolLayer
from yowsup.layers.protocol_receipts           import YowReceiptProtocolLayer
from yowsup.layers.protocol_acks               import YowAckProtocolLayer
from yowsup.layers.network                     import YowNetworkLayer
from yowsup.layers.coder                       import YowCoderLayer
from yowsup.common import YowConstants
from yowsup.layers import YowLayerEvent
from yowsup.stacks import YOWSUP_CORE_LAYERS #FIXME
from yowsup.stacks import YowStack
from yowsup import env

sys.path.insert(1,"%s/../../mosbot" % os.path.dirname(os.path.realpath(__file__)))
from config import Config

cfg=Config()

# DEBUG
if cfg.debug=="1":

  logging.basicConfig(filename=cfg.logfile,level=logging.DEBUG,
            format='%(levelname)-8s %(message)s')
else:
  logging.basicConfig(filename=cfg.logfile,level=logging.INFO,
            format='%(levelname)-8s %(message)s')

whatsapp_id=sys.argv[1]
whatsapp_pwd=sys.argv[2]

#print whatsapp_id
#print whatsapp_pwd

CREDENTIALS = (whatsapp_id,whatsapp_pwd)

cfg=Config()


if __name__==  "__main__":

    layers = (
        ReceiveLayer,
        (YowAuthenticationProtocolLayer, YowMessagesProtocolLayer, YowReceiptProtocolLayer, YowAckProtocolLayer)
    ) + YOWSUP_CORE_LAYERS

    stack = YowStack(layers)
    stack.setProp(YowAuthenticationProtocolLayer.PROP_CREDENTIALS, CREDENTIALS)         #setting credentials
    stack.setProp(YowNetworkLayer.PROP_ENDPOINT, YowConstants.ENDPOINTS[0])    #whatsapp server address
    stack.setProp(YowCoderLayer.PROP_DOMAIN, YowConstants.DOMAIN)              
    stack.setProp(YowCoderLayer.PROP_RESOURCE, env.CURRENT_ENV.getResource())          #info about us as WhatsApp client

    stack.broadcastEvent(YowLayerEvent(YowNetworkLayer.EVENT_STATE_CONNECT))   #sending the connect signal

    stack.loop(timeout=10,count=6) #this is the program mainloop

		
