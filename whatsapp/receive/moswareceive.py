#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
	mosmonitor.py
	MosBot monitoring class
	2015/01/10
	Marcelo Hartmann Terres <mhterres@gmail.com>
"""

import os
import sys
import time
import datetime
import logging

import subprocess 
from threading import Thread

import multiprocessing

class mosWAReceive(Thread):

	def __init__(self,bot):

		Thread.__init__(self)

		self.description = 'WhatsApp Message Receiver'

		self.bot = bot

	def run(self):

		time.sleep(10)

		while True:

			logging.debug("WhatsApp: checking messages")
			saida=self.runProcess(True)
			self.sendmsgs(saida)

			logging.debug("WhatsApp: waiting 30 seconds to start receiving messages again")
			time.sleep(30)

	def runProcess(self,valor):    

		logging.debug("WhatsApp: starting receiver process")

		pathname=os.path.dirname(sys.argv[0])
		path=os.path.abspath(pathname)

		logging.debug("WhatsApp: %s %s" % (pathname,path))

		cmd=["%s/whatsapp/receive/receive.py" % path,self.bot.config.whatsapp_id,self.bot.config.whatsapp_pwd]

		logging.debug("WhatsApp: cmd %s" % cmd)

		p = subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)

		logging.debug("WhatsApp: starting receiver process - returning data")
		return p.communicate()[0]

	def sendmsgs(self,saida):

		messages=saida.split("\n")

		for message in messages:

			if len(message.strip())>0:

				x=0
				msg=""

				data=message.split(" ")

				for item in data:

					if x==4:

						number=item[:-1]

					if x > 4:

						if x > 5:

							msg=msg+" "
					
						msg+=item

					x=x+1


				if msg.split(" ")[0].lower()==self.bot.config.botname.lower():

					logging.debug("WhatsApp: message to %s" % self.bot.config.botname)
				else:

					self.msg("%s@%s" % (self.bot.config.roomname,self.bot.config.confsrv),message,"groupchat")

				if len(msg.strip())>0:

					self.bot.dbpgsql.saveRecWAMessage(number,msg)


	def msg(self,msg_to,msg,msg_type):

		self.bot.send_message(mto=msg_to,mbody=msg,mtype=msg_type)


		

