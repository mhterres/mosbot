--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

ALTER TABLE ONLY public.whatsapp_send DROP CONSTRAINT whatsapp_send_pkey;
ALTER TABLE ONLY public.whatsapp_rec DROP CONSTRAINT whatsapp_rec_pkey;
ALTER TABLE ONLY public.users_logs DROP CONSTRAINT users_logs_pkey;
ALTER TABLE ONLY public.muc_chat DROP CONSTRAINT logs_pkey;
DROP TABLE public.whatsapp_send;
DROP SEQUENCE public.whatsapp_send_id_seq;
DROP TABLE public.whatsapp_rec;
DROP SEQUENCE public.whatsapp_rec_id_seq;
DROP TABLE public.users_logs;
DROP SEQUENCE public.users_logs_id_seq;
DROP TABLE public.muc_chat;
DROP SEQUENCE public.logs_id_seq;
DROP EXTENSION plpgsql;
DROP SCHEMA public;
--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: logs_id_seq; Type: SEQUENCE; Schema: public; Owner: mos_mucbot
--

CREATE SEQUENCE logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 10000000
    CACHE 1;


ALTER TABLE public.logs_id_seq OWNER TO mos_mucbot;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: muc_chat; Type: TABLE; Schema: public; Owner: mos_mucbot; Tablespace: 
--

CREATE TABLE muc_chat (
    id bigint DEFAULT nextval('logs_id_seq'::regclass) NOT NULL,
    date timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    nick character varying(50) NOT NULL,
    msg_from character varying(150),
    message text NOT NULL,
    msg_to character varying(150)
);


ALTER TABLE public.muc_chat OWNER TO mos_mucbot;

--
-- Name: users_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: mos_mucbot
--

CREATE SEQUENCE users_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 10000000
    CACHE 1;


ALTER TABLE public.users_logs_id_seq OWNER TO mos_mucbot;

--
-- Name: users_logs; Type: TABLE; Schema: public; Owner: mos_mucbot; Tablespace: 
--

CREATE TABLE users_logs (
    id bigint DEFAULT nextval('users_logs_id_seq'::regclass) NOT NULL,
    date timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone,
    nick character varying(50) NOT NULL,
    operation character varying(15) NOT NULL,
    jid character varying(150)
);


ALTER TABLE public.users_logs OWNER TO mos_mucbot;

--
-- Name: whatsapp_rec_id_seq; Type: SEQUENCE; Schema: public; Owner: mos_mucbot
--

CREATE SEQUENCE whatsapp_rec_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 10000000000
    CACHE 1;


ALTER TABLE public.whatsapp_rec_id_seq OWNER TO mos_mucbot;

--
-- Name: whatsapp_rec; Type: TABLE; Schema: public; Owner: mos_mucbot; Tablespace: 
--

CREATE TABLE whatsapp_rec (
    id bigint DEFAULT nextval('whatsapp_rec_id_seq'::regclass) NOT NULL,
    date timestamp without time zone DEFAULT now() NOT NULL,
    src character varying(50) NOT NULL,
    message text NOT NULL,
    answered boolean,
    needs_answer boolean
);


ALTER TABLE public.whatsapp_rec OWNER TO mos_mucbot;

--
-- Name: whatsapp_send_id_seq; Type: SEQUENCE; Schema: public; Owner: mos_mucbot
--

CREATE SEQUENCE whatsapp_send_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 10000000000
    CACHE 1;


ALTER TABLE public.whatsapp_send_id_seq OWNER TO mos_mucbot;

--
-- Name: whatsapp_send; Type: TABLE; Schema: public; Owner: mos_mucbot; Tablespace: 
--

CREATE TABLE whatsapp_send (
    id bigint DEFAULT nextval('whatsapp_send_id_seq'::regclass) NOT NULL,
    date timestamp without time zone DEFAULT now() NOT NULL,
    dst character varying(50) NOT NULL,
    message text NOT NULL,
    auto boolean
);


ALTER TABLE public.whatsapp_send OWNER TO mos_mucbot;

--
-- Name: logs_pkey; Type: CONSTRAINT; Schema: public; Owner: mos_mucbot; Tablespace: 
--

ALTER TABLE ONLY muc_chat
    ADD CONSTRAINT logs_pkey PRIMARY KEY (id);


--
-- Name: users_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: mos_mucbot; Tablespace: 
--

ALTER TABLE ONLY users_logs
    ADD CONSTRAINT users_logs_pkey PRIMARY KEY (id);


--
-- Name: whatsapp_rec_pkey; Type: CONSTRAINT; Schema: public; Owner: mos_mucbot; Tablespace: 
--

ALTER TABLE ONLY whatsapp_rec
    ADD CONSTRAINT whatsapp_rec_pkey PRIMARY KEY (id);


--
-- Name: whatsapp_send_pkey; Type: CONSTRAINT; Schema: public; Owner: mos_mucbot; Tablespace: 
--

ALTER TABLE ONLY whatsapp_send
    ADD CONSTRAINT whatsapp_send_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

