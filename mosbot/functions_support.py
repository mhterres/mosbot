# -*- coding: utf-8 -*-

import datetime
import psycopg2
import psycopg2.extras
from mosbot.config import Config

def tktsFromRT(rt_queue):

    cfg=Config()

    # initial connection to RT
    t = rt.Rt(cfg.rt_url_rest, cfg.rt_user, cfg.rt_password)
    t.login()

    # get open|new|stalled tkts from queue ordered by LastUpdate desc
    activeTkts = t.search(Queue=rt_queue, order='-Created',raw_query="Status = 'new' OR Status = 'open' OR Status = 'stalled'")

    # get resolved tkts from queue ordered by LastUpdate desc, return only
    # those updated after dateSearchResolved
    dateSearchResolved = datetime.date.today() - datetime.timedelta(int(cfg.days_search_resolved))
    closedTkts = t.search(Queue=rt_queue, order='-LastUpdated',raw_query=("Status = 'resolved' AND LastUpdated > '%s'" % dateSearchResolved.strftime("%Y-%m-%d")))

    i=1
    activeTktsSummary = ''
    for tkt in activeTkts:

        if i <= int(cfg.display_tkt_count):
    
            activeTktsSummary += "%i - rt#%s (%s) %s (%s)\r" % (i, tkt['id'].split('/')[1],tkt['Owner'], tkt['Subject'], tkt['Status'])
            activeTktsSummary +="%s\r" % (cfg.rt_url_display % tkt['id'].split('/')[1])
        else:

            break
        
        i=i+1

    activeTktsSummary += '\r'

    i=1
    closedTktsSummary = ''
    for tkt in closedTkts:

        if i <= int(cfg.display_tkt_count):
            closedTktsSummary += "%i - rt#%s (%s) %s\r" % (i, tkt['id'].split('/')[1],tkt['Owner'], tkt['Subject'])
            closedTktsSummary +="%s\r" % (cfg.rt_url_display % tkt['id'].split('/')[1])
        else:
            break

        i=i+1

    closedTktsSummary += '\r'

    return (activeTktsSummary, closedTktsSummary)

def getAsteriskRealtimeInformation(jid):

    cfg = Config()

    dsn = 'dbname=%s host=%s user=%s password=%s' % (cfg.ast_db_name, cfg.ast_db_host, cfg.ast_db_user, cfg.ast_db_pwd)

    conn = psycopg2.connect(dsn)
    curs = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    sql = "SELECT name,callerid,%s from %s where %s='%s';" % (cfg.ast_jid_field,cfg.ast_sip_table,cfg.ast_jid_field,jid)
    curs.execute(sql,)

    if not curs.rowcount:
        return ["","",""]

    row=curs.fetchone()
    name = row['name']
    callerid = row['callerid']
    return [name,jid,callerid]

