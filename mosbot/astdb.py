#!/usr/bin/env python
# -*- coding: utf-8 -*-

class astDB():

    def __init__(self,ami):

        self.description = 'Asterisk Database'
        self.ami=ami

    def get(self,key,extension):

        return self.ami.database_get("exten_%s" % extension, key)

    def put(self,key,extension,value):

        cDict = {'Action':'DBPut'}
        cDict['Family'] = "exten_%s" % extension
        cDict['Key'] = key
        cDict['Val'] = value

        response = self.ami.send_action(cDict)
        return response

    def rem(self,key,extension):

        cDict = {'Action':'DBDel'}
        cDict['Family'] = "exten_%s" % extension
        cDict['Key'] = key

        response = self.ami.send_action(cDict)
                
        return response

