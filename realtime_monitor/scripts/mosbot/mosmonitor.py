#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
	mosmonitor.py
	MosBot monitoring class
	2015/01/10
	Marcelo Hartmann Terres <mhterres@gmail.com>
"""

import os
import sys
import glob
import time
import datetime
import logging
import ConfigParser

from threading import Thread
from subprocess import Popen, PIPE, STDOUT

from mosbot.config import Config

class mosMonitor(Thread):

	def __init__(self,bot,num):

		Thread.__init__(self)
		self.num = num

		self.description = 'MOSbot monitor'

		pathname = os.path.dirname(sys.argv[0])
		path=os.path.abspath(pathname)

		self.monitor_path="%s/monitor/" % path
		self.monitor_scripts_path="%s/monitor/scripts/" % path

		self.bot = bot

		files = glob.glob(self.monitor_path + "*.mon")

		monitors=[]

		for file in files:

			parser=ConfigParser.RawConfigParser()
			parser.read(file)

			monitors.append([parser.get("general","name"),parser.get("general","script"),parser.get("general","message"),parser.get("general","newmessage"),parser.get("general","check"),parser.get("general","integer"),"",""])

		self.monitors=monitors

		self.check(True,"","")

	def run(self):

		cfg=Config()
	
		while True:

			logging.debug("Checking monitoring items every one minute...")

			self.check(False,"%s@%s" % (cfg.roomname,cfg.confsrv),"groupchat")
			time.sleep(60)

	def check(self,init,msg_to,msg_type):
		
		now = time.time()

		i = 0

		for monitor in self.monitors:

			check = False

			if not init:

				if now - int(monitor[6].split(".")[0]) > int(monitor[4]):

					check = True

			new = False

			if monitor[7] == "":

				new=True

			if init or check:

				cmd="%s/%s" % (self.monitor_scripts_path,monitor[1])
				p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
				output = p.stdout.read().replace("\r","").replace("\n","")
				logging.debug("Running %s - Output %s)" % (cmd,output))

				if not init:

					if not new:

						if monitor[5]=="1":
					
							if output != monitor[7]:

								diff=int(output) - int(monitor[7])
								self.msg(msg_to,monitor[3] % str(diff),msg_type)
						else:

							if monitor[7] != output:

								self.msg(msg_to,monitor[3] % output,msg_type)

				self.monitors[i][6]=str(now)
				self.monitors[i][7]=output

			i=i+1

	def showMonitors(self,msg_to,msg_type):

		if len(self.monitors)==0:

			output="No monitors available."
		else:

			output="Monitors available:"

			for monitor in self.monitors:

				output+="\n%s - %s - %s" % (datetime.datetime.fromtimestamp(int(monitor[6].split(".")[0])).strftime('%Y-%m-%d %H:%M'),monitor[0],monitor[2] % monitor[7])

		self.msg(msg_to,output,msg_type)

	def msg(self,msg_to,msg,msg_type):

		self.bot.send_message(mto=msg_to,mbody=msg,mtype=msg_type)

		

