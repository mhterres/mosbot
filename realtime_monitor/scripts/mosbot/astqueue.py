#!/usr/bin/python
# -*- coding: utf-8 -*-

import time

from mosbot.astdb import astDB
from mosbot.classes_support import connectAMI
from mosbot.classes_support import ManagerDict

class astQueue:

        def __init__(self,queue,ami):

                self.description = 'Asterisk Queue'

                myConnect=connectAMI()
                s = myConnect.socket

                astdb=astDB(ami)
                self.astdb=astdb

                s.send('Action: QueueSummary\nQueue: ' + queue + '\n\n')
                time.sleep (0.1)

                data = s.recv(65536)

                if data.find('Queue: ' + queue) == -1:

                        self.isavailable=False
                else:

                        self.isavailable=True

                        mgrdict=ManagerDict(data)

                        self.name=queue
                        self.loggedmembers=mgrdict.getitem('LoggedIn')
                        self.availablemembers=mgrdict.getitem('Available')
                        self.members=[]

                s.close

        def getmembers(self):

                myConnect=connectAMI()
                s = myConnect.socket

                s.send('Action: QueueSummary\nQueue: %s\n\n' % self.name)
                time.sleep (0.1)
                data = s.recv(65536)

                mgrdict=ManagerDict(data)

                self.loggedmembers=mgrdict.getitem('LoggedIn')
                self.availablemembers=mgrdict.getitem('Available')

                s.send('Action: QueueStatus\nQueue: ' + self.name + '\n\n')
                time.sleep (0.1)

                data2 = s.recv(65536)
                mgrdict=ManagerDict(data2)

                self.strategy = mgrdict.getitem('Strategy')
                self.calls = mgrdict.getitem('Calls')
                self.completed = mgrdict.getitem('Completed')
                self.abandoned = mgrdict.getitem('Abandoned')
                self.holdtime = mgrdict.getitem('Holdtime')
                self.talktime = mgrdict.getitem('TalkTime')

                linhas = data2.split('\n')

                aMembers=[]

                for item in linhas:

                        data=item.split(":",1)

                        try:

                                title=data[0]
                        except:

                                title=""

                        try:

                                value=data[1]
                        except:

                                value=""

                        if title == "Name":

                                aMembers.append(value)

                self.members=[]

                self.members=aMembers

                s.close

                return (aMembers)

        def ismember(self,member):

                returnValue=False

                for item in self.members:

                        if 'SIP/' + member in item:

                                returnValue=True

                return returnValue

        def addmember(self,member):

                myConnect=connectAMI()
                s = myConnect.socket

                if self.ismember(member):

                        returnmsg = "Extension %s is already a member of queue %s." % (member,self.name)
                else:

                        s.send('Action: QueueAdd\nQueue: ' + self.name + '\nInterface: SIP/' + member + '\n\n')
                        time.sleep(.2)

                        self.getmembers()

                        if self.ismember(member):

                            self.astdb.put('queue',member,self.name)
                            self.astdb.put('queuelogin',member,1)

                            returnmsg = "Extension %s is now member of queue %s." % (member,self.name)
                        else:
    
                            returnmsg = "ERROR: extension %s can't join queue %s." % (member,self.name)
                            s.close()

                return (returnmsg)

        def removemember(self,member):

                myConnect=connectAMI()
                s = myConnect.socket

                if not self.ismember(member):

                    returnmsg = "Extension %s is not member of queue %s." % (member,self.name)

                else:

                        s.send('Action: QueueRemove\nQueue: ' + self.name + '\nInterface: SIP/' + member + '\n\n')
                        time.sleep(.2)

                        self.getmembers()

                        if not self.ismember(member):

                                self.astdb.rem('queue',member)
                                self.astdb.rem('queuelogin',member)

                                returnmsg = "Extension %s leaves queue %s." % (member,self.name)
                        else:

                                returnmsg = "ERROR: extension %s can't leave queue %s." % (member,self.name)


                s.close()

                return (returnmsg)
