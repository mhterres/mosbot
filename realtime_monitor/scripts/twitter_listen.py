#!/usr/bin/python
# -*- coding: utf-8 -*-


import tweepy
import sys
from datetime import datetime
from optparse import OptionParser
from tweepy import StreamListener

optp = OptionParser()

optp.add_option('-t', '--twitter', help='set twitter account to listen',
					dest='twitter_listen_account', default="")
optp.add_option('-f', '--file', help='file containing strings to search (one per line)',
					dest='search_string_file', default="")
optp.add_option('-s', '--string', help='string to search',
					dest='search_string',default="")

opts, args = optp.parse_args()

search_string=opts.search_string
search_string_file=opts.search_string_file
twitter_listen_account=opts.twitter_listen_account

if twitter_listen_account=="":

	print "You need to inform twitter account to listen (option -t)."
	sys.exit(1)

if (search_string=="" and search_string_file==""):
	print "You need to inform string to search (option -s) or file containing search strings (option -f)."
	sys.exit(1)

if search_string=="":

	with open(search_string_file) as f:
    
		strings = f.readlines()
else:

	strings=[search_string]	

consumer_key="tWHH9GndJsW0f1CmHBBKOVySF"
consumer_secret="bNZPtRe31m76tK2sSZ2ce19EMRveRdOGtVCplDc7yGdZOJTWfp"

access_token="10840182-PZ1l9e8gAV9LeyRJzlPPbQVtA9CnJ9TBLTgnLLDjD"
access_token_secret="pc2PxTneVtDSa9uu26U2XAatLiL4ezP10ASkWu5H7RBzm"

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth)

class StdOutListener(StreamListener):
    ''' Handles data received from the stream. '''
 
    def on_status(self, status):
        # Prints the text of the tweet
        print(status.text)
 
        return True
 
    def on_error(self, status_code):
        print('Got an error with status code: ' + str(status_code))
        return True # To continue listening
 
    def on_timeout(self):
        print('Timeout...')
        return True # To continue listening
 
if __name__ == '__main__':
    listener = StdOutListener()
    stream = tweepy.streaming.Stream(auth, listener)
    stream.filter(follow=[twitter_listen_account], track=strings)
    #stream.filter(track=strings)
